### Librerías
import RPi.GPIO as GPIO
import time

### Configuración pines Raspberry Pi
#https://tutorials-raspberrypi.com/raspberry-pi-ultrasonic-sensor-hc-sr04/
#https://thepihut.com/blogs/raspberry-pi-tutorials/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi
# GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
set GPIO Pins
GPIO_TRIGGER_front = 6
GPIO_ECHO_front = 13
GPIO_TRIGGER_der = 0
GPIO_ECHO_der = 5
GPIO_TRIGGER_izq = 19
GPIO_ECHO_izq = 26
# set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER_front, GPIO.OUT)
GPIO.setup(GPIO_ECHO_front, GPIO.IN)
GPIO.setup(GPIO_TRIGGER_der, GPIO.OUT)
GPIO.setup(GPIO_ECHO_der, GPIO.IN)
GPIO.setup(GPIO_TRIGGER_izq, GPIO.OUT)
GPIO.setup(GPIO_ECHO_izq, GPIO.IN)
# inicialización
GPIO.output(GPIO_TRIGGER_front, False)
GPIO.output(GPIO_TRIGGER_der, False)
GPIO.output(GPIO_TRIGGER_izq, False)

### Definición de la función para obtener la distancia a partir de los ultrasonidos
def distance(GPIO_TRIGGER, GPIO_ECHO):
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
    # StartTime = time.time()
    # StopTime = time.time()
    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()
    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2 # en cm
    return distance

### Definición de la función de Control Reactivo
def control_reactivo():
    # Lectura sensores ultrasonios ¡¡¡¡ en cm !!!!
    #PRUEBA
    #PRUEBA = 50#30#
    #frontalUR = PRUEBA
    #derUR = PRUEBA
    #izqUR = PRUEBA
    #REALES
    frontalUR = distance(GPIO_TRIGGER_front, GPIO_ECHO_front)
    derUR = distance(GPIO_TRIGGER_der, GPIO_ECHO_der)
    izqUR = distance(GPIO_TRIGGER_izq, GPIO_ECHO_izq)
    # ¿¿¿ Publicar en ROS los datos de los ultrasonidos ???
    
    # Límite distancia seguridad
    dist_seg = 40
    
    # Si no hay ninguna detección continúa realizando la navegación
    fin_naveg = False
    
    # Bucle al que entra si hay alguna detección
    while frontalUR < dist_seg or derUR < dist_seg or izqUR < dist_seg:
        fin_naveg = True
        if derUR < dist_seg or izqUR < dist_seg:
            if derUR < dist_seg and izqUR < dist_seg:
                print('Parar robot')
            elif izqUR < dist_seg:
                print('Desplazamiento lateral del robot hacia la DERECHA')
            else: #derUR < dist_seg
                print('Desplazamiento lateral del robot hacia la IZQUIERDA')
        elif dist_seg/2 <= frontalUR < dist_seg:
            print('Reducir velocidad 50%')
        else: #frontalUR < dist_seg/2
            print('Parar robot + Desplazamiento lateral')
            if izqUR <= derUR:
                print('Desplazamiento lateral del robot hacia la DERECHA')
            else: #izqUR > derUR
                print('Desplazamiento lateral del robot hacia la IZQUIERDA')
            
        # Lectura sensores ultrasonios ¡¡¡¡ en cm !!!!
        #PRUEBA
        #print('valor del US frontal')
        #frontalUR = int(input())
        #print('valor del US derecho')
        #derUR = int(input())
        #print('valor del US izquierdo')
        #izqUR = int(input())
        #REALES
        frontalUR = distance(GPIO_TRIGGER_front, GPIO_ECHO_front)
        derUR = distance(GPIO_TRIGGER_der, GPIO_ECHO_der)
        izqUR = distance(GPIO_TRIGGER_izq, GPIO_ECHO_izq)
        # ¿¿¿ Publicar en ROS los datos de los ultrasonidos ???
        
    return fin_naveg

#PRUEBA
#fin_naveg = control_reactivo()
#print(fin_naveg)

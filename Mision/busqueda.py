#!/usr/bin/env python

### Librerias
import rospy
import math

# Variables globales
busqueda = 0
sentido_lider = -1 # 0 izq <-, 1 dcha ->, -1 No info
sentido_persA = -1 # 0 izq <-, 1 dcha ->, -1 No info
objetivo = 1

def busqueda_lider(num_i,giro):
    if num_i < 405/giro:
        theta = giro/360*2*math.pi
        d_planif = 0
        vel = 0
    else:
        theta = 0
        d_planif = 2
        vel = 0.5
    return theta, d_planif, vel

def giro_robot(giro, sentido):
    if sentido == 0: # Izquierda
        theta = - giro/360*2*math.pi
    else: #Derecha 
        theta = giro/360*2*math.pi
    return theta

def busqueda_nodo():
    global busqueda, sentido_lider, sentido_persA, objetivo
    # Subscribers: topic /busqueda, /sentido_lider, /sentido_persA /objetivo
    # Publishers: angulo, distancia, /perseguidor_b/vel_lineal
    
    lider = 1
    perseguidor = 3
    giro = 7.5 
    
    num_i = 0
    num_g = 0
    while not rospy.is_shutdown():
        if busqueda:
            if objetivo == lider:
                sentido = sentido_lider
            else:
                sentido = sentido_persA
                
            ### Algoritmo
            if sentido != -1 and num_g < 360/giro: # Hay info
                theta = giro_robot(giro, sentido)
                d_planif = 0
                vel = 0
                num_g+=1
            else:
                sentido_lider, sentido_persA = -1, -1 # No hay info
                theta, d_planif, vel = busqueda_lider(num_i,giro)
                num_i = num_i + 1 if num_i<405/giro else 0
                num_g = 0
                
            ### Publicar datos en ROS: theta, d_planif, vel
        

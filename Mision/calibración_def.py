### LIBRERÍAS
import cv2
from huskylib import HuskyLensLibrary
import pickle
import numpy as np

############################################################################################################
### OBTENER LOS PARÁMETROS DE CALIBRACIÓN DE LA CÁMARA
'''
Enlace: https://docs.opencv.org/master/da/d13/tutorial_aruco_calibration.html
'''

############################################################################################################
### Función que obtiene las charucoCorners, los charucoIds y el board de la imagen proporcionada

def puntos (datos, imagen):
    #####################################################################
    ### Creación de la ChArUco Board (Apriltags)
    '''
    retval = cv2.aruco.CharucoBoard_create(squaresX, squaresY, squareLength, markerLength, dictionary)

    Parameters
        squaresX      number of chessboard squares in X direction
        squaresY      number of chessboard squares in Y direction
        squareLength  chessboard square side length (normally in meters)
        markerLength  marker side length (same unit than squareLength)
        dictionary    dictionary of markers indicating the type of markers. The first markers in the dictionary are used to fill the white chessboard squares.

    Returns
        the output CharucoBoard object

    Enlace: https://docs.opencv.org/master/d0/d3c/classcv_1_1aruco_1_1CharucoBoard.html
    '''
    squaresX = 3
    squaresY = 3
    squareLength = 0.06
    markerLength = 0.05
    '''
    retval = cv.aruco.getPredefinedDictionary(dict)

    Parameter
        dict     PREDEFINED_DICTIONARY_NAME

    Return
        Returns one of the predefined dictionaries defined in PREDEFINED_DICTIONARY_NAME.

    Enlace: https://docs.opencv.org/master/d9/d6a/group__aruco.html
    '''
    dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_APRILTAG_36h11);
    
    board = cv2.aruco.CharucoBoard_create(squaresX, squaresY, squareLength, markerLength, dictionary)
    
    ## Crear las imagen de la ChArUco Board  (Apriltags)
    '''
    retorno = board.draw((tamaño_ancho, tamaño_alto))

    Parámetros
        tamaño_ancho  tamaño del ancho de la imagen en píxeles
        tamaño_alto   tamaño del alto de la imagen en píxeles
        (Si no son proporcionales a las dimensiones de la tabla, la imagen se centrará)

    Enlace: https://mecaruco2.readthedocs.io/en/latest/notebooks_rst/Aruco/sandbox/ludovic/aruco_calibration_rotation.html
    '''
    charuco_board = board.draw((680, 680))
    ## Mostrar la ChArUco Board  (Apriltags)
    cv2.imshow("CharUco_Board", charuco_board)
    ## Guardar la ChArUco Board  (Apriltags)
    #cv2.imwrite('CharApriltag_Board_2.png', charuco_board)
    
    #####################################################################
    ### Tamaño de la imagen de la cámara
    ## Leer imagen
    img = cv2.imread(imagen)#('2.bmp')
    ## Mostrar la imagen tomada
    cv2.imshow("img_camara_calib", img)
    h,w,_ = img.shape
    #print(img.shape) #'''(height,width)'''
    
    #####################################################################
    ### Leer los datos del sensor de visión
    # Lectura en modo binario 
    fichero = open(datos,'rb')
    # Cargamos los datos del fichero
    detection = pickle.load(fichero)
    #print(detection)
    # Cerrar ficheros
    fichero.close()
    
    #####################################################################
    ### Obtener las esquinas y las ID de los marcadores
    board_corners_array = [0,0,0,0]
    cont = 0
    for block in detection:
        coord_x_centro = block.__dict__['x']
        #print(coord_x_centro)
        coord_y_centro = block.__dict__['y']
        #print(coord_y_centro)
        ancho = block.__dict__['width']
        #print(ancho)
        alto = block.__dict__['height']
        #print(alto)
        ID_detect = cont
        
        top_left_corner = [coord_x_centro-(ancho/2),coord_y_centro+(alto/2)]
        top_right_corner = [coord_x_centro+(ancho/2),coord_y_centro+(alto/2)]
        bottom_right_corner = [coord_x_centro+(ancho/2),coord_y_centro-(alto/2)]
        bottom_left_corner = [coord_x_centro-(ancho/2),coord_y_centro-(alto/2)]
        
        corners = [top_left_corner, top_right_corner, bottom_right_corner, bottom_left_corner]
        #print(corners)
        #print(type(corners))
        
        if (cont == 0):
            board_corners = [corners]
            #print(board_corners)
            board_corners_array[cont] = np.array(board_corners, dtype='f4')
            board_corners_array
            board_ID = [ID_detect]
        else:
            #board_corners.append(corners) #'''Enlace: https://devcode.la/tutoriales/listas-python/'''
            #print(board_corners)
            board_corners = [corners]
            #print(board_corners)
            board_corners_array[cont] = np.array(board_corners, dtype='f4')
            #print(board_corners_array)
            board_ID.append(ID_detect)
        
        cont = cont+1

    #print('board_corners')
    #print(board_corners)
    #board_corners_array = np.array(board_corners, dtype='f4')
    #print('board_corners_array')
    #print(board_corners_array)
    #print(board_ID)
    #board_ID = [[0],[1],[2],[3]]
    board_ID_array = np.array(board_ID, dtype='i')
    #print(board_ID_array)

    #####################################################################
    ### Interpolar las esquinas de los Apriltags para obtener las esquinas ChArUco
    '''
    retval, charucoCorners, charucoIds = cv.aruco.interpolateCornersCharuco( markerCorners, markerIds, image, board, charucoCorners,
                                                                            charucoIds, cameraMatrix, distCoeffs, minMarkers)

    Interpolate position of ChArUco board corners.

    Parameters
        markerCorners   vector of already detected markers corners. For each marker, its four corners are provided, (e.g std::vector<std::vector<cv::Point2f> > ).
                        For N detected markers, the dimensions of this array should be Nx4. The order of the corners should be clockwise.
        markerIds       list of identifiers for each marker in corners
        image           input image necesary for corner refinement. Note that markers are not detected and should be sent in corners and ids parameters.
        board           layout of ChArUco board.
        charucoCorners  interpolated chessboard corners
        charucoIds      interpolated chessboard corners identifiers
        cameraMatrix    optional 3x3 floating-point camera matrix A=[fx 0 cx; 0 fy cy; 0 0 1]
        distCoeffs      optional vector of distortion coefficients (k1,k2,p1,p2[,k3[,k4,k5,k6],[s1,s2,s3,s4]]) of 4, 5, 8 or 12 elements
        minMarkers      number of adjacent markers that must be detected to return a charuco corner

    This function receives the detected markers and returns the 2D position of the chessboard corners from a ChArUco board using the detected Aruco markers.
    If camera parameters are provided, the process is based in an approximated pose estimation, else it is based on local homography. Only visible corners
    are returned. For each corner, its corresponding identifier is also returned in charucoIds. The function returns the number of interpolated corners. 

    Enlace: https://docs.opencv.org/master/d9/d6a/group__aruco.html
    '''
    corners = board_corners_array
    ids = board_ID_array
    charucoCorners = np.array([])
    charucoIds = np.array([])
    retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco( corners, ids, img, board, charucoCorners, charucoIds)
    #print()
    #print(charucoCorners)
    #print()
    #print(charucoIds)

    ## Crear una copia de la imagen tomada
    detection_img_3 = img.copy()
    ## Crear la imagen tomada con los Apriltags detectados
    detection_img_3_ = cv2.aruco.drawDetectedCornersCharuco(detection_img_3, charucoCorners, charucoIds, [0,255,0]) #----> No sé por qué no funciona
    ## Mostrar la imagen tomada con los Apriltags detectados
    cv2.imshow("img_con_detecciones_3", detection_img_3)
    #cv2.imshow("img_con_detecciones_3_", detection_img_3_) #Lo mismo

    return charucoCorners, charucoIds, board

############################################################################################################
### Recopilación de todas las charucoCorners y los charucoIds de todas las imágenes

for i in [16,18,19,20,21,23,24]:
    if i==16:
        charucoCorners16, charucoIds16, board = puntos ('obj16.pickle', '16.bmp')
#         print()
#         print()
#         print('charucoCorners16')
#         print(charucoCorners16)
#         print()
#         print('charucoIds16')
#         print(charucoIds16)
#         print()
#         print()
         
        group_corners = [charucoCorners16]
#         print('group_corners16')
#         print(group_corners)
#         print()
        group_ID = [charucoIds16]
#         print('group_ID16')
#         print(group_ID)
#         print()
        
    elif i==18:
        charucoCorners18, charucoIds18, board = puntos ('obj18.pickle', '18.bmp')
#         print()
#         print()
#         print('charucoCorners18')
#         print(charucoCorners18)
#         print()
#         print('charucoIds18')
#         print(charucoIds18)
#         print()
#         print()
         
        group_corners.append(charucoCorners18) #'''Enlace: https://devcode.la/tutoriales/listas-python/'''
#         print('group_corners16+18')
#         print(group_corners)
#         print()
        group_ID.append(charucoIds18)
#         print('group_ID16+18')
#         print(group_ID)
#         print()

    elif i==19:
        charucoCorners19, charucoIds19, board = puntos ('obj19.pickle', '19.bmp')
#         print()
#         print()
#         print('charucoCorners19')
#         print(charucoCorners19)
#         print()
#         print('charucoIds19')
#         print(charucoIds19)
#         print()
#         print()
         
        group_corners.append(charucoCorners19)
#         print('group_corners16+18+19')
#         print(group_corners)
#         print()
        group_ID.append(charucoIds19)
#         print('group_ID16+18+19')
#         print(group_ID)
#         print()
        
    elif i==20:
        charucoCorners20, charucoIds20, board = puntos ('obj20.pickle', '20.bmp')
#         print()
#         print()
#         print('charucoCorners20')
#         print(charucoCorners20)
#         print()
#         print('charucoIds20')
#         print(charucoIds20)
#         print()
#         print()
        
        group_corners.append(charucoCorners20)
#         print('group_corners16+18+19+20')
#         print(group_corners)
#         print()
        group_ID.append(charucoIds20)
#         print('group_ID16+18+19+20')
#         print(group_ID)
#         print()
        
    elif i==21:
        charucoCorners21, charucoIds21, board = puntos ('obj21.pickle', '21.bmp')
#         print()
#         print()
#         print('charucoCorners21')
#         print(charucoCorners21)
#         print()
#         print('charucoIds21')
#         print(charucoIds21)
#         print()
#         print()
         
        group_corners.append(charucoCorners21)
#         print('group_corners16+18+19+20+21')
#         print(group_corners)
#         print()
        group_ID.append(charucoIds21)
#         print('group_ID16+18+19+20+21')
#         print(group_ID)
#         print()
        
    elif i==23:
        charucoCorners23, charucoIds23, board = puntos ('obj23.pickle', '23.bmp')
#         print()
#         print()
#         print('charucoCorners23')
#         print(charucoCorners23)
#         print()
#         print('charucoIds23')
#         print(charucoIds23)
#         print()
#         print()
         
        group_corners.append(charucoCorners23)
#         print('group_corners16+18+19+20+21+23')
#         print(group_corners)
#         print()
        group_ID.append(charucoIds23)
#         print('group_ID16+18+19+20+21+23')
#         print(group_ID)
#         print()

    elif i==24:
        charucoCorners24, charucoIds24, board = puntos ('obj24.pickle', '24.bmp')
#         print()
#         print()
#         print('charucoCorners24')
#         print(charucoCorners24)
#         print()
#         print('charucoIds24')
#         print(charucoIds24)
#         print()
#         print()
        
        group_corners.append(charucoCorners24)
#         print('group_corners16+18+19+20+21+23+24')
#         print(group_corners)
#         print()
        group_ID.append(charucoIds24)
#         print('group_ID16+18+19+20+21+23+24')
#         print(group_ID)
#         print()

############################################################################################################
### Obtener los prámetros de calibración de la cámara del sensor
'''
retval, cameraMatrix, distCoeffs, rvecs, tvecs = cv.aruco.calibrateCameraCharuco( charucoCorners, charucoIds, board, imageSize, cameraMatrix, distCoeffs,
                                                                                rvecs, tvecs, flags, criteria)

Calibrate a camera using Charuco corners.

Parameters
    charucoCorners  vector of detected charuco corners per frame
    charucoIds      list of identifiers for each corner in charucoCorners per frame
    board           Marker Board layout
    imageSize       input image size
    cameraMatrix    Output 3x3 floating-point camera matrix A=[fx 0 cx; 0 fy cy; 0 0 1] . If CV_CALIB_USE_INTRINSIC_GUESS and/or CV_CALIB_FIX_ASPECT_RATIO
                    are specified, some or all of fx, fy, cx, cy must be initialized before calling the function.
    distCoeffs      Output vector of distortion coefficients (k1,k2,p1,p2[,k3[,k4,k5,k6],[s1,s2,s3,s4]]) of 4, 5, 8 or 12 elements
    rvecs           Output vector of rotation vectors (see Rodrigues ) estimated for each board view (e.g. std::vector<cv::Mat>>).
                    That is, each k-th rotation vector together with the corresponding k-th translation vector (see the next output parameter description)
                    brings the board pattern from the model coordinate space (in which object points are specified) to the world coordinate space, that is,
                    a real position of the board pattern in the k-th pattern view (k=0.. M -1).
    tvecs           Output vector of translation vectors estimated for each pattern view.
    flags           flags Different flags for the calibration process (see calibrateCamera for details).
    criteria        Termination criteria for the iterative optimization algorithm.

This function calibrates a camera using a set of corners of a Charuco Board. The function receives a list of detected corners and its identifiers from
several views of the Board. The function returns the final re-projection error. 

Enlace: https://docs.opencv.org/master/d9/d6a/group__aruco.html
'''
imageSize=(320,240)#(w,h)
cameraMatrix = np.zeros((3,3))
distCoeffs = np.array([])
retval, cameraMatrix, distCoeffs, rvecs, tvecs = cv2.aruco.calibrateCameraCharuco( group_corners, group_ID, board, imageSize, cameraMatrix, distCoeffs)

print()
print()
print('cameraMatrix')
print(cameraMatrix)
print()
print()
print('distCoeffs')
print(distCoeffs)
print()
print()

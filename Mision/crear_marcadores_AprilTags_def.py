# Librerías
import numpy as np
import cv2

#Definir el diccionario: marcadores Apriltag 6x6 - distancia 11
dictionary=cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_APRILTAG_36h11);
#Definir las matrices para las imágenes de los marcadores (10x10)
img_marcador_0 = np.zeros((378, 378, 1), dtype="uint8")
img_marcador_1 = np.zeros((378, 378, 1), dtype="uint8")
#Crear las imágenes de los marcadores: tamaño de la imagen 378x378 píxeles (10x10 cm)
cv2.aruco.drawMarker(dictionary,0,378,img_marcador_0,1) # 1er marcador - Líder y saboteador
cv2.aruco.drawMarker(dictionary,1,378,img_marcador_1,1) # 2o  marcador - Perseguidor A y B
#Mostrar las imágenes de los marcadores
cv2.imshow("img_marcador_0", img_marcador_0) # 1er marcador - Líder y saboteador
cv2.imshow("img_marcador_1", img_marcador_1) # 2o  marcador - Perseguidor A y B
#Guardar las imágenes de los marcadores
cv2.imwrite('img_marcador_april_0.png', img_marcador_0) # 1er marcador - Líder y saboteador
cv2.imwrite('img_marcador_april_1.png', img_marcador_1) # 2o  marcador - Perseguidor A y B

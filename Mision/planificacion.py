### Librerías
import math

# Inicializar variables globales
pose_lider_x = 0.
pose_lider_y = 0.
pose_pers_x = 0.
pose_pers_y = 0.
objetivo = 1
busqueda = 0


def planificacion():
    ### Obtencion datos de ROS: 
    #   - /lider/pose, /perseguidor_a/pose
    #   - /objetivo
    #   - /busqueda  

    # IDs
    lider = 1
    perseguidor = 3
    
    while not rospy.is_shutdown():
        if not busqueda
            # Obtencion id del objetivo
            id_obj = objetivo
            # Obtencion datos de ROS            
            #Pose robots detectados
            if (id_obj == lider):
                x_obj = pose_lider_x
                y_obj = pose_lider_y
            else: #id_obj == perseguidor
                x_obj = pose_pers_x
                y_obj = pose_pers_y

            ### Calculo de la distancia y angulo
            d=math.sqrt(x_obj**2+y_obj**2)
            if y_obj == 0:
                theta = 0
            else:
                theta = math.atan(x_obj/y_obj)

            ### Cálculo de la velocidad y la distancia al punto planificado

            ### Calculo de la velocidad y la distancia al punto planificado
            ## Distancias segun el objetivo para regular la velocidad
            if (id_obj == lider):
                d_1 = 0.90
                d_2 = 0.95
                d_3 = 1.05
            else: #id_obj == perseguidor
                d_1 = 0.50
                d_2 = 0.55
                d_3 = 0.65

            if(d < d_1):
                vel = 0
                d_planif = 0
            elif (d < d_2):
                vel = (d-d_1)*0.4/0.05
                d_planif = 30*d/100
            elif (d < d_3):
                vel = 0.4+((d-d_2)*0.2/0.1) 
                d_planif = 30*d/100
            else: 
                vel = 0.6
                d_planif = 30*d/100


            ### Publicar datos en ROS: theta d_planif y velocidad
return
    

#!/usr/bin/env python

import cv2
import math
import numpy as np

dist_pers = 0
dist_lider = 0


# PARAMETROS CONSTANTES
markerLength = 0.05
cameraMatrix = [[614.12543824, 0., 158.22657736],[ 0., 696.44962538 ,117.71536189],[ 0., 0., 1.]]
distCoeffs = [[ 1.72000936e+00],[-8.44224186e+01],[ 7.53673976e-02],[ 3.07264497e-01],[ 1.33930345e+03]]

# Funciones auxiliares 
def adaptFormatBbox(bbox):
    [xc, yc, w, h, ID] = bbox
    return {[xc-w/2,yc-h/2],[xc+w/2,yc-h/2],[xc+w/2,yc+h/2],[xc-w/2,yc+h/2]}

def getPose(corners):
    # Calcular pose marcador
    _, tvecs, _= cv2.aruco.estimatePoseSingleMarkers(np.float32([corners]), markerLength,
                                                     np.float32(cameraMatrix), np.float32(distCoeffs))
    x = tvecs[0][0][0]
    y = tvecs[0][0][2]
    #theta = math.atan(x/y)
    return x,y


def estimacion():
    ### Obtener datos ROS: lider/bbox y perseguidorA/bbox

    # Misma implementacion para lider y perseguidor
    # Adaptar formato: Depende como introduzcais datos se adapta esto
    corners = adaptFormatBbox(bbox)
    x, y = getPose(corners)
    ### Publicar datos en ROS. Posicion lider y perseguidor A: (x,y)

    ### Seleccion objetivo
    dist_lider = math.sqrt(x**2 + y**2)
    dist_pers = math.sqrt(x**2 + y**2)

    if dist_pers == 0:
            objetivo = 1 # Lider
        elif dist_lider == 0:
            objetivo = 3 # Perseguidor
        elif dist_lider > dist_pers:
            objetivo = 3 # Perseguidor
        else:
            objetivo = 1 # Lider

    ### Publicar datos en ROS. ID Objetivo

    return 

#!/usr/bin/env python

### Librerias
import rospy
from std_msgs.msg import UInt8, Float32
from geometry_msgs.msg import Point
from chaseb.msg import MiPose
import math

# Inicializar variables globales
pose_lider_x = 0.
pose_lider_y = 0.
pose_pers_x = 0.
pose_pers_y = 0.
objetivo = 1
busqueda = 0

def callback_lider(msg):
    global pose_lider_x, pose_lider_y
    rospy.loginfo("Pose lider: {}".format(msg))
    pose_lider_x = msg.x
    pose_lider_y = msg.y

def callback_pers(msg):
    global pose_pers_x, pose_pers_y
    rospy.loginfo("Pose pers: {}".format(msg))
    pose_pers_x = msg.x
    pose_pers_y = msg.y

def callback_objetivo(msg):
    global objetivo
    rospy.loginfo("Objetivo: {}".format(msg.data))
    objetivo = msg.data

def callback_busqueda(msg):
    global busqueda
    rospy.loginfo("Busqueda: {}".format(msg.data))
    busqueda = msg.data

def planificacion():
    global pose_lider_x, pose_lider_y, pose_pers_x, pose_pers_y, objetivo, busqueda
    # Se inicializa el nodo 
    rospy.init_node('planificacion', anonymous=True)
    
    # Este nodo de suscribe a la pose de ambos robots, al objetivo y a la busqueda
    subLP = rospy.Subscriber('/lider/pose',Point,callback_lider)
    subPP = rospy.Subscriber('/perseguidor_a/pose',Point,callback_pers)
    subO =  rospy.Subscriber('/objetivo',UInt8,callback_objetivo)
    subB = rospy.Subscriber('/busqueda',UInt8,callback_busqueda)

    # Este nodo publica ditancia, angulo y velocidad lineal 
    pubDist = rospy.Publisher('/distancia',Float32,queue_size=10) 
    pubAng = rospy.Publisher('/angulo',Float32,queue_size=10)
    pubVel = rospy.Publisher('/perseguidor_b/vel_lineal',Float32,queue_size=10)
    r = rospy.Rate(10) #2hz
    
    # Identificadores de los robots
    lider = 1
    perseguidor = 3

    while not rospy.is_shutdown():
        if not busqueda: 
            # Obtencion id del objetivo
            id_obj = objetivo

            # Obtencion datos de ROS
            # Pose robots detectados
            if (id_obj == lider):
                x_obj = pose_lider_x
                y_obj = pose_lider_y
            else: #id_obj == perseguidor
                x_obj = pose_pers_x
                y_obj = pose_pers_y

            # Calculo de la distancia y angulo
            d=math.sqrt(x_obj**2+y_obj**2)
            if y_obj == 0:
                theta = 0
            else:
                theta = math.atan(x_obj/y_obj)

            # Calculo de la velocidad y la distancia al punto planificado
            # Distancias segun el objetivo para regular la velocidad. 
            if (id_obj == lider):
                d_1 = 0.9
                d_2 = 0.95
                d_3 = 1.05
            else: #id_obj == perseguidor
                d_1 = 0.50
                d_2 = 0.55
                d_3 = 0.65

            # Se adapta la velocidad en función de la distancia entre la correspondiente al punto planificado
            # y la del relativa al robot a perseguir
            if(d < d_1):
                vel = 0 
                d_planif = 0
                theta = 0
            elif (d < d_2):
                vel = (d-d_1)*0.4/0.05
                d_planif = 30*d/100
            elif (d < d_3):
                vel = 0.4+((d-d_2)*0.2/0.1) 
                d_planif = 30*d/100
            else: 
                vel = 0.6
                d_planif = 30*d/100

            # Publicar datos en ROS: theta d_planif y velocidad
            dist_plan = Float32()
            dist_plan.data = d_planif
            pubDist.publish(dist_plan)    
            ang_plan = Float32()
            ang_plan.data = theta
            pubAng.publish(ang_plan)   
            velocidad = Float32()
            velocidad.data = vel
            pubVel.publish(velocidad)
            rospy.loginfo('Distancia planificada: {}'.format(dist_plan.data))
            rospy.loginfo('Angulo: {}'.format(ang_plan.data))
            rospy.loginfo('Velocidad: {}'.format(velocidad.data))
         

        r.sleep()

if __name__ == '__main__':
    try:
        planificacion()
    except rospy.ROSInterruptException: pass

#!/usr/bin/python3

### Librerias
import rospy
import math
from std_msgs.msg import UInt8, Float32
from geometry_msgs.msg import Point

# Esta condicion se aplica cuando los subscribers se hacen con variables
busqueda = 0
sentido_lider = -1 # 0 izq <-, 1 dcha ->, -1 Si no recibe información
sentido_persA = -1 # 0 izq <-, 1 dcha ->, -1 Si no recibe información
objetivo = 1

def busqueda_lider(num_i,giro):
    if num_i < 405/giro: #Si no tiene objetivo, se pone a buscar al lider
        theta = giro/360*2*math.pi # Realiza una vuelta completa si no ha detectado al líder
        d_planif = 0
        vel = 0
    else:
        theta = 0
        d_planif = 0.5
        vel = 0.1
    return theta, d_planif, vel

def giro_robot(giro, sentido): # Esta función se aplica si has perdido al lider o perseguidor
    print("Giro robot")
    if sentido == 0: # Izquierda
        theta = - giro/360*2*math.pi
        print("Sentido izquierda")
    elif sentido == 1: #Derecha 
        theta = giro/360*2*math.pi
    return theta

def callback_objetivo(msg):
    global objetivo
    rospy.loginfo("Objetivo: {}".format(msg.data))
    objetivo = msg.data

def callback_busqueda(msg):
    global busqueda
    rospy.loginfo("Busqueda: {}".format(msg.data))
    busqueda = msg.data

def callback_sentP(msg):
    global sentido_persA
    rospy.loginfo("Sentido perseguidor: {}".format(msg.data))
    sentido_persA = msg.data

def callback_sentL(msg):
    global sentido_lider
    rospy.loginfo("Sentido lider: {}".format(msg.data))
    sentido_lider = msg.data

def busqueda_nodo(): 
    global busqueda, sentido_lider, sentido_persA, objetivo
    # Subscribers: topic /busqueda, /sentido_lider, /sentido_persA /objetivo
    # Publishers: angulo, distancia, /perseguidor_b/vel_lineal

    # Se inicializa el nodo 
    rospy.init_node('busqueda', anonymous=True)
    
    # Se suscribe al sentido de ambos robots, objetivo y busqueda
    sub_sentidoL = rospy.Subscriber('/lider/sentido',UInt8,callback_sentL)
    sub_sentidoP = rospy.Subscriber('/perseguidor_a/sentido',UInt8,callback_sentP)
    subO =  rospy.Subscriber('/objetivo',UInt8,callback_objetivo)
    subB = rospy.Subscriber('/busqueda',UInt8,callback_busqueda)

    # Publica distancia, angulo y velocidad lineal
    pubDist = rospy.Publisher('/distancia',Float32,queue_size=10) 
    pubAng = rospy.Publisher('/angulo',Float32,queue_size=10)
    pubVel = rospy.Publisher('/perseguidor_b/vel_lineal',Float32,queue_size=10)
    r = rospy.Rate(2) #2hz    
    lider = 1
    perseguidor = 3
    giro = 7.5 
    
    num_i = 0
    num_g = 0
    while not rospy.is_shutdown():
        if busqueda:
            if objetivo == lider:
                sentido = sentido_lider
            else:
                sentido = sentido_persA
                
            # Algoritmo
            if sentido != -1 and num_g < 360/giro: # Hay informacion
                print("Entra en el if de giro")
                theta = giro_robot(giro, sentido)
                d_planif = 0
                vel = 0
                num_g+=1
            else:
                sentido_lider, sentido_persA = -1, -1 # No hay info
                theta, d_planif, vel = busqueda_lider(num_i,giro)
                num_i = num_i + 1 if num_i<405/giro else 0
                num_g = 0

            ### Publicar datos en ROS: theta d_planif y velocidad
            dist_plan = Float32()
            dist_plan.data = d_planif
            pubDist.publish(dist_plan)    
            ang_plan = Float32()
            ang_plan.data = theta
            pubAng.publish(ang_plan)   
            velocidad = Float32()
            velocidad.data = vel
            pubVel.publish(velocidad)
            rospy.loginfo('Distancia planificada: {}'.format(dist_plan.data))
            rospy.loginfo('Angulo: {}'.format(ang_plan.data))
            rospy.loginfo('Velocidad: {}'.format(velocidad.data))
                
        r.sleep()

if __name__ == '__main__':
    try:
        busqueda_nodo()
    except rospy.ROSInterruptException: pass
        

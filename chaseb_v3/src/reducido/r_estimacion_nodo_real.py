#!/usr/bin/env python3

import rospy
from std_msgs.msg import String
from std_msgs.msg import UInt8
from std_msgs.msg import Float32
from geometry_msgs.msg import Point
import time
import math

import cv2
import math
import numpy as np

from chaseb.msg import BoundingBox

# Se inicializan variables
dist_pers = 0
dist_lider = 0
objetivo = 1

# Parametros constantes
markerLength = 0.05
cameraMatrix = [[614.12543824, 0., 158.22657736],[ 0., 696.44962538 ,117.71536189],[ 0., 0., 1.]]
distCoeffs = [[ 1.72000936e+00],[-8.44224186e+01],[ 7.53673976e-02],[ 3.07264497e-01],[ 1.33930345e+03]]

# Funciones auxiliares 
def adaptFormatBbox(bbox):
    # [xc, yc, w, h, ID] = bbox
    xc = int(bbox[0])
    yc = int(bbox[1])
    w = int(bbox[2])
    h = int(bbox[3])
    ID = int(bbox[4])
    return [[xc-w/2,yc-h/2],[xc+w/2,yc-h/2],[xc+w/2,yc+h/2],[xc-w/2,yc+h/2]]

def getPose(corners):
    # Calcular pose marcador
    _, tvecs, _= cv2.aruco.estimatePoseSingleMarkers(np.float32([corners]), markerLength,
                                                     np.float32(cameraMatrix), np.float32(distCoeffs))
    x = tvecs[0][0][0]
    y = tvecs[0][0][2]
    return x,y


def callbackL(bbox):
    global dist_lider
    bboxList = list(bbox.data.split(","))
    corners = adaptFormatBbox(bboxList)
    
    if bboxList[4] == 0:
        dist_lider = 0.
    else:
        x, y = getPose(corners)
             
        ### Publicar datos en ROS. Posicion lider y perseguidor A:
        pubL = rospy.Publisher('/lider/pose',Point,queue_size=10)
        pubLD = rospy.Publisher('/lider/distancia',Float32,queue_size=10)

        pose_lider = Point()
        pose_lider.x = x 
        pose_lider.y = y
        pubL.publish(pose_lider)

        dist_lider = math.sqrt(pose_lider.x**2 + pose_lider.y**2)
        rospy.loginfo("Distancia lider: {}".format(dist_lider))
        pubLD.publish(dist_lider) 

def callbackP(bbox):
    global dist_pers
    bboxList = list(bbox.data.split(","))
    corners = adaptFormatBbox(bboxList)

    if bboxList[4] == 0:
        dist_pers = 0.
    else:
        x, y = getPose(corners)     
  
        ### Publicar datos en ROS.
        pubP = rospy.Publisher('/perseguidor_a/pose',Point,queue_size=10)
        pubPD = rospy.Publisher('/perseguidor_a/distancia',Float32,queue_size=10)
            
        pose_pers = Point()
        pose_pers.x = x
        pose_pers.y = y
        pubP.publish(pose_pers) 

        dist_pers = math.sqrt(pose_pers.x**2 + pose_pers.y**2)
        rospy.loginfo("Distancia perseguidor: {}".format(dist_pers))
       
    

def estimacion():
    global dist_lider, dist_pers, objetivo
    rospy.init_node('estimacion', anonymous=True)
    
    # Se suscribe a los topics de bbox de ambos robots y el objetivo
    subL = rospy.Subscriber('/lider/bbox',String,callbackL)
    subP = rospy.Subscriber('/perseguidor_a/bbox',String,callbackP)
    pubO = rospy.Publisher('/objetivo',UInt8,queue_size=10)

    r = rospy.Rate(2) #2hz

    while not rospy.is_shutdown():
        rospy.loginfo('dist_pers = {}'.format(dist_pers))
        rospy.loginfo('dist_lider = {}' .format(dist_lider))
        
        # Determina el objetivo a partir de su dispancia a ambos robots
        if dist_pers < 0.01:
            objetivo = 1 # Lider
        elif dist_lider < 0.01:
             objetivo = 3 #Perseguidor            
        elif dist_lider > dist_pers:
            objetivo = 3 # Perseguidor
        elif dist_pers > dist_lider:
            objetivo = 1 # Lider
        else:
            objetivo = 1
           
        pubO.publish(objetivo)
        rospy.loginfo("Objetivo: {}".format(objetivo))
        
        r.sleep()
         

if __name__ == '__main__':
    try:
        estimacion()
    except rospy.ROSInterruptException: pass

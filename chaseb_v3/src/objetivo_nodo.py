#!/usr/bin/python3

import rospy
from std_msgs.msg import String
from std_msgs.msg import UInt8
from std_msgs.msg import Float32
from geometry_msgs.msg import Point
import time

dist_pers = 0
dist_lider = 0

def callback_dist_pers(msg):
    global dist_pers
    rospy.loginfo("Distancia pers: {}".format(msg))
    dist_pers = msg.data

def callback_dist_lider(msg):
    global dist_lider
    rospy.loginfo("Distancia lider: {}".format(msg))
    dist_lider = msg.data   

# Esta funcion tiene que devolver objetivo para poder utilizarlo en el algoritmo de planificacion

def objetivo():
    global dist_lider, dist_pers
    rospy.init_node('objetivo', anonymous=True)
    subLD = rospy.Subscriber('/lider/distancia',Float32,callback_dist_lider)
    subPD = rospy.Subscriber('/perseguidor_a/distancia',Float32,callback_dist_pers)
    pubO = rospy.Publisher('/objetivo',UInt8,queue_size=10)
    r = rospy.Rate(2) #2hz
    while not rospy.is_shutdown():        
        if dist_pers == 0:
            objetivo = 1 # Lider
        elif dist_lider == 0:
            objetivo = 3 # Perseguidor
        elif dist_lider > dist_pers:
            objetivo = 3 # Perseguidor
        else:
            objetivo = 1 # Lider

        ### Publicar datos en ROS. Posicion lider y perseguidor A Tipo:
        pubO.publish(objetivo)
        rospy.loginfo("Objetivo: {}".format(objetivo))

        r.sleep()

if __name__ == '__main__':
    try:
        objetivo()
    except rospy.ROSInterruptException: pass

# Python3
# Librerías
import RPi.GPIO as GPIO
import time
import serial
import threading

# Configuración pines Raspberry Pi
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# Pins GPIO
GPIO_TRIGGER_front = 6 
GPIO_ECHO_front = 13
led_front = 20
GPIO_TRIGGER_der = 0
GPIO_ECHO_der = 5
led_der = 16
GPIO_TRIGGER_izq = 19
GPIO_ECHO_izq = 26
led_izq = 21

# Set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER_front, GPIO.OUT)
GPIO.setup(GPIO_ECHO_front, GPIO.IN)
GPIO.setup(GPIO_TRIGGER_der, GPIO.OUT)
GPIO.setup(GPIO_ECHO_der, GPIO.IN)
GPIO.setup(GPIO_TRIGGER_izq, GPIO.OUT)
GPIO.setup(GPIO_ECHO_izq, GPIO.IN)
GPIO.setup(led_front, GPIO.OUT)
GPIO.setup(led_izq, GPIO.OUT)
GPIO.setup(led_der, GPIO.OUT)

# Inicialización
GPIO.output(GPIO_TRIGGER_front, False)
GPIO.output(GPIO_TRIGGER_der, False)
GPIO.output(GPIO_TRIGGER_izq, False)
GPIO.output(led_front,GPIO.LOW)
GPIO.output(led_izq,GPIO.LOW)
GPIO.output(led_der,GPIO.LOW)

fin_naveg = False

# Interrupción cada 1 segundo
def Timer_Interrupt():
    controlreactivo()
    threading.Timer(0.6, Timer_Interrupt).start() ##NOTA: hacer que sea 0.5

# Trayectoria punto a punto
def trayectoria():

    mov = 0
    
    while True:
        global fin_naveg
        
        if fin_naveg == True:
            fin_naveg = False
            mov = '0'
            
            
        # Si el robot finaliza la trayectoria planificada
        if arduino.inWaiting() > 0:
            mov = arduino.read().decode(encoding='ascii')
        
        # Generar una nueva trayectoria
        if (mov == '0'):
            [distancia, giro, vel_avance_d] = planificacion()

            # Sentido del giro
            if giro > 0:
                dir_giro = "g"     # Giro derecha

            if giro < 0:
                dir_giro = "G"     # Giro Izquierda

            # Tiempo y velocidad de giro
            vel_giro = 100                         # Velocidad 0 a 255
            vel_giro_d = (100*600)/255             # Velocidad en mm/s
            giro = abs(giro)
            if giro == 0:
                giro = 0.01
            tiempo_giro = (giro*1000) / vel_giro
            tiempo_giro = int(tiempo_giro) 

            # Mover robot linealmente
            vel_avance = (255*vel_avance_d)/600    # Velocidad de 0 a 255
            tiempo_avance = (distancia*1000) / vel_avance_d
            tiempo_avance = int(tiempo_avance)
            vel_avance = int(vel_avance)
            
            # Envía trayecto a arduino
            info_tray = "T,"+str(dir_giro)+","+str(vel_avance)+","+str(tiempo_giro)+","+str(tiempo_avance)
            print(info_tray)
            arduino.write(info_tray.encode())
            time.sleep(0.0001)

            # Activa movimiento
            mov = 1


def planificacion():
    distancia = 50
    giro = 90
    vel = 100
    return distancia, giro, vel


def controlreactivo():
    
    # Límite distancia seguridad
    dist_seg = 40
    
    # Lectura sensores ultrasonidos en ¡¡cm!!
    frontalUR = distance(GPIO_TRIGGER_front, GPIO_ECHO_front)
    if (frontalUR < dist_seg):
        GPIO.output(led_front,GPIO.HIGH) #LEDs amarillos en el robot
    else:
        GPIO.output(led_front,GPIO.LOW)
        
    derUR = distance(GPIO_TRIGGER_der, GPIO_ECHO_der)
    if (derUR < dist_seg):
        GPIO.output(led_der,GPIO.HIGH)
    else:
        GPIO.output(led_der,GPIO.LOW)
        
    izqUR = distance(GPIO_TRIGGER_izq, GPIO_ECHO_izq)
    if (izqUR < dist_seg):
        GPIO.output(led_izq,GPIO.HIGH)
    else:
        GPIO.output(led_izq,GPIO.LOW)
    
    global fin_naveg
    naveg = False
        
    # Si existe algún obstáculo
    while frontalUR < dist_seg or derUR < dist_seg or izqUR < dist_seg:
        naveg = True
        #si se detectó un obstáculo detener el robot justo antes de iniciar CR
        #print('Se detectó un obstáculo, detener robot')
        #dir = 'P'
        #vel = 0
        #info_tray = "C," + str(dir) + "," + str(vel) 
        #print(info_tray)
        #arduino.write(info_tray.encode())
        #time.sleep(0.5)
        
        if derUR < dist_seg or izqUR < dist_seg:
            if derUR < dist_seg and izqUR < dist_seg: #si tiene obst. a los 2 lados, parar
                print('Parar robot obstáculos ambos costados')
                dir = 'P'
                vel = 0
            elif izqUR < dist_seg:
                print('Desplazamiento lateral del robot hacia la DERECHA')
                dir = 'h'
                vel = 20
            else: #derUR < dist_seg 
                print('Desplazamiento lateral del robot hacia la IZQUIERDA')
                dir = 'H'
                vel = 20
        elif dist_seg/2 <= frontalUR < dist_seg: #si detecta algo al frente reducir vel.
            print('Reducir velocidad 50%')
            dir = 'A'
            vel = 10
        else: #frontalUR < dist_seg/2
            print('Parar robot + Desplazamiento lateral')
            dir = 'P'
            vel = 0
            info_tray = "C," + str(dir) + "," + str(vel) 
            print(info_tray)
            arduino.write(info_tray.encode())
            print("Ejecuta control reactivo")
            time.sleep(0.7)
            
            if izqUR <= derUR and frontalUR < dist_seg/2:
                print('Desplazamiento lateral del robot hacia la DERECHA')
                dir = 'h'
                vel = 20
            elif izqUR > derUR and frontalUR < dist_seg/2:
                print('Desplazamiento lateral del robot hacia la IZQUIERDA')
                dir = 'H'
                vel = 20
        
        # Envía trayecto de control reactivo a arduino
        info_tray = "C," + str(dir) + "," + str(vel) 
        print(info_tray)
        arduino.write(info_tray.encode())
        time.sleep(1)
        
        frontalUR = distance(GPIO_TRIGGER_front, GPIO_ECHO_front)
        derUR = distance(GPIO_TRIGGER_der, GPIO_ECHO_der)
        izqUR = distance(GPIO_TRIGGER_izq, GPIO_ECHO_izq)
    
    if naveg == True:
        naveg = False
        time.sleep(1.5)
        fin_naveg = True
        
        

# Distancia a partir de los ultrasonidos
def distance(GPIO_TRIGGER, GPIO_ECHO):
     
     GPIO.output(GPIO_TRIGGER, True)
     
     time.sleep(0.00001)
     GPIO.output(GPIO_TRIGGER, False)
     
     while GPIO.input(GPIO_ECHO) == 0:
         StartTime = time.time()
         
     while GPIO.input(GPIO_ECHO) == 1:
         StopTime = time.time()
     TimeElapsed = StopTime - StartTime
     
     distance = (TimeElapsed * 34300) / 2
     
     return distance



if __name__=='__main__':
    
    arduino = serial.Serial('/dev/ttyUSB0', 115200)
    time.sleep(0.1)
    
    if arduino.isOpen():
        threading.Timer(0.6, Timer_Interrupt).start() #NOTA:hacer que sea 0.5
        trayectoria()
    arduino.close()

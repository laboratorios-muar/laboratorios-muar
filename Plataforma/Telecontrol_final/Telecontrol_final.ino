#include <PinChangeInt.h>
#include <PinChangeIntConfig.h>
#define _NAMIKI_MOTOR   //for Namiki 22CL-103501PG80:1

#include <fuzzy_table.h>
#include <PID_Beta6.h>

#ifndef MICROS_PER_SEC
#define MICROS_PER_SEC 1000000
#endif
/*

            \                    /
   wheel1   \                    /   wheel4
   Left     \                    /   Right
    
    
                              power switch
    
            /                    \
   wheel2   /                    \   wheel3
   Right    /                    \   Left

 */


// DEFINIR RUEDAS
// Rueda 1 (UL)
int R1= 11; 
int D1= 12;
// Rueda 2 (LL)
int R2= 3; 
int D2= 2; 
// Rueda 3 (LR)
int R3= 10; 
int D3= 7;
// Rueda 4 (UR)
int R4= 9; 
int D4= 8; 

int t = 0;
char tipo;

void setup() {

  Serial.begin(9600);

  // Definir pines como salida
  pinMode(R1,OUTPUT); 
  pinMode(D1,OUTPUT); 
  pinMode(R2,OUTPUT); 
  pinMode(D2,OUTPUT); 
  pinMode(R3,OUTPUT); 
  pinMode(D3,OUTPUT); 
  pinMode(R4,OUTPUT); 
  pinMode(D3,OUTPUT);   

  TCCR2B = TCCR2B & 0b11111000 | 0x01;
     
}

void loop() {
  int vel = 200;
  
  // Obtiene los valores del telecontrol
  if (Serial.available()>0){
      // Se lee la direccion enviada
      char direccion = Serial.read();
    
      if(direccion == 'w') {
        mov_robot ('A', vel);
        Serial.println("Avance");   
      }
      if(direccion =='x') {
        mov_robot ('R', vel);
        Serial.println("Hacia atrás");
      }
      if(direccion =='s') {
        mov_robot ('P', 0);
        Serial.println("Stop"); 
      }
      if(direccion =='a') {
        mov_robot ('H', vel);
        Serial.println("izquierda");
      }
      if(direccion =='d') {
        mov_robot ('h', vel);
        Serial.println("derecha");
      }
      if(direccion =='e') {
        mov_robot ('d', vel);
        Serial.println("diagonal hacia delante derecha");
      }
      if(direccion =='q') {
        mov_robot ('D', vel);
        Serial.println("diagonal hacia delante izquierda");
      }
      if(direccion =='z') {
        mov_robot ('L', vel);
        Serial.println("diagonal hacia atrás izquierda");
      }
      if(direccion =='c') {
        mov_robot ('l', vel);
        Serial.println("Diagonal hacia atrás derecha");
      }
      if(direccion =='j') {
        mov_robot ('G', vel);
        Serial.println("rotación antihoraria");
      }
      if(direccion =='l') {
        mov_robot ('g', vel);
        Serial.println("Rotación horaria");
      }
        }
   //aquí faltaría mandar el mov=0 a la rbpi
}


void mov_robot (char dir, int vel) 
{
  switch (dir) {
        // Movimiento de avance
        case 'A':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de retroceso
        case 'R':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de rotación izquierda
        case 'G':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'g':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'D':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,vel);
            analogWrite(R3,0);
            analogWrite(R4,vel);
            break;
        case 'd':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,0);
            analogWrite(R3,vel);
            analogWrite(R4,0);
            break;
        case 'L':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,0);
            analogWrite(R3,vel);
            analogWrite(R4,0);
            break;
        case 'l':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,vel);
            analogWrite(R3,0);
            analogWrite(R4,vel);
            break;
        case 'H':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'h':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,LOW);
            digitalWrite(D3,LOW);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'P':            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,0);
            analogWrite(R3,0);
            analogWrite(R4,0);
            break;
        default:
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,0);
            analogWrite(R3,0);
            analogWrite(R4,0);
            break;
        }
}

#include <PinChangeInt.h>
#include <PinChangeIntConfig.h>
#define _NAMIKI_MOTOR	 //for Namiki 22CL-103501PG80:1

#include <fuzzy_table.h>
#include <PID_Beta6.h>

#ifndef MICROS_PER_SEC
#define MICROS_PER_SEC 1000000
#endif
/*
            \                    /
   wheel1   \                    /   wheel4
   Left     \                    /   Right
    
    
                              power switch
    
            /                    \
   wheel2   /                    \   wheel3
   Right    /                    \   Left
*/
// DEFINIR RUEDAS
// Rueda 1 (UL)
int R1= 11; 
int D1= 12;
// Rueda 2 (LL)
int R2= 3; 
int D2= 2; 
// Rueda 3 (LR)
int R3= 10; 
int D3= 7;
// Rueda 4 (UR)
int R4= 9; 
int D4= 8; 

int t = 0;
char tipo;
//LEDs control
int led_tray = 4;
int led_cr = 5;

void setup() {
  Serial.begin(9600);
  // Definir pines como salida
  pinMode(R1,OUTPUT); 
  pinMode(D1,OUTPUT); 
  pinMode(R2,OUTPUT); 
  pinMode(D2,OUTPUT); 
  pinMode(R3,OUTPUT); 
  pinMode(D3,OUTPUT); 
  pinMode(R4,OUTPUT); 
  pinMode(D3,OUTPUT);   
  pinMode(led_tray, OUTPUT);
  pinMode(led_cr, OUTPUT);
  TCCR2B = TCCR2B & 0b11111000 | 0x01;     
}

void loop() {

  // Obtiene los valores de la raspberry
  if (Serial.available()>0){
    String vector = Serial.readString();
    //String vector = "T,g,2,4,6";

    // Tipo de movimiento
    tipo = vector.charAt(0);

    if (tipo == 'T'){
        digitalWrite(led_tray,HIGH); //LED blanco de control para saber sí se está ejecutando una trayectoria
        trayectoria(vector);
        digitalWrite(led_tray,LOW);
    }
    if (tipo == 'C'){
        digitalWrite(led_cr, HIGH); //LED rojo de control para saber si se está ejecutando el control reactivo
        controlreactivo(vector);
        digitalWrite(led_cr, LOW);
    }  
  }    
}

void trayectoria(String vector){

    int CR = 0;
    //String vector = "T,g,2,4,6";

    // Direcciones
    char dir_giro = vector.charAt(2);
    char dir_avance = 'A';

    // Velocidades
    int cont = 4;
    for (int i=4;1<vector.length();i++){
      if (vector.charAt(cont) == ','){
        break;
      }
      cont++;
    }
    cont = cont + 1;
    String vel = vector.substring(4,cont);
    int vel_avance = vel.toInt();
    Serial.println(vel_avance);
    int vel_giro = 100;
     
    // Tiempo
    int inicial = cont;
    for (int i=cont;1<vector.length();i++){
      if (vector.charAt(cont) == ','){
        break;
      }
      cont++;
    }
    cont = cont + 1;
    String tiempoG = vector.substring(inicial,cont);
    int tiempo_giro = tiempoG.toInt();
    Serial.println(tiempo_giro);

    inicial = cont;
    cont = vector.length();
    String tiempoA = vector.substring(inicial,cont);
    int tiempo_avance = tiempoA.toInt();
    Serial.println(tiempo_avance);
        
    mov_robot(dir_giro,vel_giro);
    
    t  = millis();
    while(1){
      if ((millis() - t) > tiempo_giro){
        break;
      }
      
      // Si se activa el control reactivo
      if (Serial.available()>0){
        vector=Serial.read();

        // Tipo de movimiento
        tipo = vector.charAt(0); 
        
        if (tipo == 'C'){
            controlreactivo(vector);
            CR = 1;
            break;
        }
      }      
    }  

    if (CR == 0) {
        mov_robot(dir_avance,vel_avance);
    
    t  = millis();
    while(1){
      if ((millis() - t) > tiempo_avance){
        mov_robot('P', 0);
        break;
      } 
    
      // Si se activa el control reactivo
      if (Serial.available()>0){
        vector=Serial.read();

        // Tipo de movimiento
        tipo = vector.charAt(0); 
        
        if (tipo == 'C'){
            controlreactivo(vector);
            break;
        }
      }
      }      
    }
}


      
void controlreactivo(String vector){

    //String vector = "C,g,vel,tiempo";

    // Direcciones
    char dir_cr = vector.charAt(2);

    // Velocidades
    int cont = 4;
    for (int i=4;1<vector.length();i++){
      if (vector.charAt(cont) == ','){
        break;
      }
      cont++;
    }
    cont = cont + 1;
    String vel = vector.substring(4,cont);
    int vel_cr = vel.toInt();
     
    // Tiempo
    int inicial = cont;
    cont = vector.length();
    String tiempoA = vector.substring(inicial,cont);
    int tiempo_cr = tiempoA.toInt();
    Serial.println(tiempo_cr);

    mov_robot(dir_cr, vel_cr);
    
    t  = millis();
    while(1){
      if ((millis() - t) > tiempo_cr){
        mov_robot('P', 0);
        break;
      }    
    }
}


void mov_robot (char dir, int vel) 
{
  switch (dir) {
        // Movimiento de avance
        case 'A':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de retroceso
        case 'R':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de rotación izquierda
        case 'G':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'g':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'D':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,vel);
            analogWrite(R3,0);
            analogWrite(R4,vel);
            break;
        case 'd':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,0);
            analogWrite(R3,vel);
            analogWrite(R4,0);
            break;
        case 'H':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'h':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,LOW);
            digitalWrite(D3,LOW);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        case 'P':            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,0);
            analogWrite(R3,0);
            analogWrite(R4,0);
            break;
        default:
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,0);
            analogWrite(R3,0);
            analogWrite(R4,0);
            break;
        }
}

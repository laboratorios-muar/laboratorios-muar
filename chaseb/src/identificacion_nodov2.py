#!/usr/bin/env python

import rospy
import time
from chaseb.msg import BoundingBox
from std_msgs.msg import String

bbox_sabot = ''
bbox_lider = ''

### Funciones auxiliares formato

def bboxString2Int(bbox):
    # Tranforma '85,60,0,0,1' a [85,60,0,0,1]
    if(bbox == ''):
        return []
    else:
        lista = bbox.split(',')
        for i, elem in enumerate(lista):
            lista[i] = int(elem)
        return lista

def bboxInt2String(bbox):
    # Tranforma [85,60,0,0,1] a '85,60,0,0,1'
    mystr = ''
    for elem in bbox:
        mystr = mystr + str(elem) + ','
    return mystr[0:-1]

def identificacion():
    rospy.init_node('identificacion', anonymous=True)
    # hl = HuskyLensLibrary("I2C","", address=0x32)
    # añadir librería y función para pasar formato hl a formato útil
    # añadir topic 0 o 1 para indicar si detectamos algo o no para iniciar el algoritmo de búsqueda del líder
    pubL = rospy.Publisher('/lider/bbox',BoundingBox,queue_size=10)
    pubP = rospy.Publisher('/perseguidor_a/bbox',BoundingBox,queue_size=10)
    pubS = rospy.Publisher('/saboteador/bbox',BoundingBox,queue_size=10)
    r = rospy.Rate(2) #2hz
    while not rospy.is_shutdown():
        # Datos desde sensor vision Raspberry
        # detection = hl.learnedBlocks()  ADAPTANDO ESTO AUN
        # Ej entrada: 
        detection = [[85,60,0,0,1], [300,222,0,0,1], [160,120,0,0,2]]

        # Obtener datos previos de ROS: bbox_sabot y bbox_lider
        prev_bbox_sabot = bboxString2Int(bbox_sabot)
        prev_bbox_lider = bboxString2Int(bbox_lider)

        #Definir IDs
        cod_perseguidorA = 2 
        cod_lider_sab = 1
        
        rangox = 80 # 1/4 del ancho de la pantalla
        rangoy = 60 # 1/4 del alto de la pantalla
        for bbox in detection:
            if bbox[4] == cod_perseguidorA:
                bbox[4] = 3 # Perseguidor
            else:
                # Distincion entre lider y saboteador
                if len(prev_bbox_lider) !=0:  # Lider ya identificado
                    if (prev_bbox_lider[0] - rangox < bbox[0] < rangox + prev_bbox_lider[0]
                        and (prev_bbox_lider[1] - rangoy < bbox[1] < rangoy + prev_bbox_lider[1])):
                        bbox[4] = 1 # Lider
                        prev_bbox_lider = [1]
                    else:
                        bbox[4] = 2 # Saboteador
                        prev_bbox_sabot = [1]
                else:
                    if len(prev_bbox_sabot) !=0: # Saboteador ya identificado
                        if (prev_bbox_sabot[0] - rangox < bbox[0] < rangox + prev_bbox_sabot[0]
                            and (prev_bbox_sabot[1] - rangoy < bbox[1] < rangoy + prev_bbox_sabot[1])):
                            bbox[4] = 2 # Saboteador
                            prev_bbox_sabot = [1]
                        else:
                            bbox[4] = 1 # Lider
                            prev_bbox_lider = [1]
                    else:
                        bbox[4] = 1 # Lider
                        prev_bbox_lider = [1]

        # print("Datos Raspberry procesados: ", detection)

        for elem in detection:
            bbox_str = bboxInt2String(elem[0:5])
            if bbox_str[-1] == '1':
                msgL = BoundingBox()
                # msgL.header.stamp = rospy.get_rostime()
                msgL.timestamp = rospy.get_rostime()
                msgL.data = bbox_str
                pubL.publish(msgL) # deteccion lider
                rospy.loginfo('Lider Detectado')
            elif bbox_str[-1] == '2':
                msgS = BoundingBox()
                # msgS.header.stamp = rospy.get_rostime()
                msgS.timestamp = rospy.get_rostime()
                msgS.data = bbox_str
                pubS.publish(msgS)  # deteccion sabot
                rospy.loginfo('Saboteador Detectado')
            else:
                msgP = BoundingBox()
                # msgP.header.stamp = rospy.get_rostime()
                msgP.timestamp = rospy.get_rostime()
                msgP.data = bbox_str
                pubP.publish(msgP) # deteccion perseguidor A
                rospy.loginfo('Perseguidor Detectado')
        
        r.sleep()


if __name__ == '__main__':
    try:
        identificacion()
    except rospy.ROSInterruptException: pass
#Python3
# ### Librerías
# import RPi.GPIO as GPIO
import time
# import serial
import rospy
from std_msgs.msg import UInt8, Float32
# ### Configuración pines Raspberry Pi
# #https://tutorials-raspberrypi.com/raspberry-pi-ultrasonic-sensor-hc-sr04/
# #https://thepihut.com/blogs/raspberry-pi-tutorials/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi
# GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)
# # # set GPIO Pins
# ##En el robot TRIGGER son los cables blancos, ECHO naranjas y amarillos
# ##los LEDs amarillos se encienden si el ultrasonido detecta obstáculo
# GPIO_TRIGGER_front = 6 
# GPIO_ECHO_front = 13
# led_front = 15
# GPIO_TRIGGER_der = 0
# GPIO_ECHO_der = 5
# led_der = 14
# GPIO_TRIGGER_izq = 19
# GPIO_ECHO_izq = 26
# led_izq = 18
# # # set GPIO direction (IN / OUT)
# GPIO.setup(GPIO_TRIGGER_front, GPIO.OUT)
# GPIO.setup(GPIO_ECHO_front, GPIO.IN)
# GPIO.setup(GPIO_TRIGGER_der, GPIO.OUT)
# GPIO.setup(GPIO_ECHO_der, GPIO.IN)
# GPIO.setup(GPIO_TRIGGER_izq, GPIO.OUT)
# GPIO.setup(GPIO_ECHO_izq, GPIO.IN)
# GPIO.setup(led_front, GPIO.OUT)
# GPIO.setup(led_izq, GPIO.OUT)
# GPIO.setup(led_der, GPIO.OUT)

# # # inicialización
# GPIO.output(GPIO_TRIGGER_front, False)
# GPIO.output(GPIO_TRIGGER_der, False)
# GPIO.output(GPIO_TRIGGER_izq, False)
# 
# ### Definición de la función para obtener la distancia a partir de los ultrasonidos
# def distance(GPIO_TRIGGER, GPIO_ECHO):
#      # set Trigger to HIGH
#      GPIO.output(GPIO_TRIGGER, True)
#      # set Trigger after 0.01ms to LOW
#      time.sleep(0.00001)
#      GPIO.output(GPIO_TRIGGER, False)
#      # StartTime = time.time()
#      # StopTime = time.time()
#      # save StartTime
#      while GPIO.input(GPIO_ECHO) == 0:
#          StartTime = time.time()
#      # save time of arrival
#      while GPIO.input(GPIO_ECHO) == 1:
#          StopTime = time.time()
#      # time difference between start and arrival
#      TimeElapsed = StopTime - StartTime
#      # multiply with the sonic speed (34300 cm/s)
#      # and divide by 2, because there and back
#      distance = (TimeElapsed * 34300) / 2 # en cm
#      return distance


def controlreactivo():
    # Lectura sensores ultrasonios ¡¡¡¡ en cm !!!!
    #PRUEBA
    #PRUEBA = 50#30#
    #frontalUR = PRUEBA
    #derUR = PRUEBA
    #izqUR = PRUEBA
    #REALES
    # frontalUR = distance(GPIO_TRIGGER_front, GPIO_ECHO_front)
    dist_seg = 40
    frontalUR = 50
    if (frontalUR < dist_seg):
        GPIO.output(led_front,GPIO.HIGH) #LEDs amarillos en el robot que se encienden si se detecta obstáculo
    else:
        GPIO.output(led_front,GPIO.LOW)

    derUR = 50    
    # derUR = distance(GPIO_TRIGGER_der, GPIO_ECHO_der)
    if (derUR < dist_seg):
        GPIO.output(led_der,GPIO.HIGH)
    else:
        GPIO.output(led_der,GPIO.LOW)

    izqUR = 50     
    # izqUR = distance(GPIO_TRIGGER_izq, GPIO_ECHO_izq)
    if (izqUR < dist_seg):
        GPIO.output(led_izq,GPIO.HIGH)
    else:
        GPIO.output(led_izq,GPIO.LOW)
    # ¡¡¡¡ Publicar en ROS los datos de los ultrasonidos !!!!
    
    # Límite distancia seguridad
    
    
    # Si no hay ninguna detección continúa realizando la navegación
    fin_naveg = False

    # De cuanto es el incremento, la velocidad
    # Bucle al que entra si hay alguna detección
    while frontalUR < dist_seg or derUR < dist_seg or izqUR < dist_seg:
        fin_naveg = True
        if derUR < dist_seg or izqUR < dist_seg:
            if derUR < dist_seg and izqUR < dist_seg:
                print('Parar robot')
                dir = 'P'
            elif izqUR < dist_seg:
                print('Desplazamiento lateral del robot hacia la DERECHA')
                dir = 'h'
            else: #derUR < dist_seg #################está línea va comentada o así se queda?Fernanda 15/mayo
                print('Desplazamiento lateral del robot hacia la IZQUIERDA')
                dir = 'H'
        elif dist_seg/2 <= frontalUR < dist_seg:
            print('Reducir velocidad 50%')
        else: #frontalUR < dist_seg/2
            print('Parar robot + Desplazamiento lateral')
            dir = 'P'
            if izqUR <= derUR:
                print('Desplazamiento lateral del robot hacia la DERECHA')
                dir = 'h'
            else: #izqUR > derUR
                print('Desplazamiento lateral del robot hacia la IZQUIERDA')
                dir = 'H'
        
        ##!!!!!!!!!!!!!!!!FALTA AGREGAR LóGICA PARA DEFINIR VEL y TIEMPO
        vel=100
        tiempo=1000
        info_tray = "C," + str(dir) + "," + str(vel) + "," + str(tiempo)
        print(info_tray)
        # arduino.write(info_tray.encode())

        # Lectura sensores ultrasonios ¡¡¡¡ en cm !!!!
        #PRUEBA
        #print('valor del US frontal')
        #frontalUR = int(input())
        #print('valor del US derecho')
        #derUR = int(input())
        #print('valor del US izquierdo')
        #izqUR = int(input())
        #REALES
        #frontalUR = distance(GPIO_TRIGGER_front, GPIO_ECHO_front)
        #derUR = distance(GPIO_TRIGGER_der, GPIO_ECHO_der)
        #izqUR = distance(GPIO_TRIGGER_izq, GPIO_ECHO_izq)
        # ¡¡¡¡ Publicar en ROS los datos de los ultrasonidos !!!!
        
    return fin_naveg

def callback_dist(msg):
    global distancia
    rospy.loginfo("Recibo la distancia: {}".format(msg.data))
    distancia = msg.data

def callback_ang(msg):
    global giro
    rospy.loginfo("Recibo el ángulo: {}".format(msg.data))
    giro = msg.data

def callback_vel(msg):
    global vel_avance
    rospy.loginfo("Recibo el ángulo: {}".format(msg.data))
    vel_avance = msg.data

def trayectoria():
    global distancia, giro, vel_avance
    rospy.init_node('trayectoria', anonymous=True)
    pubDist = rospy.Subscriber('/distancia',Float32,callback_dist) # YA NO ES TRAYECTORIA ES distancia y angulo
    pubAng = rospy.Subscriber('/angulo',Float32,callback_ang)
    pubVel = rospy.Subscriber('/perseguidor_b/vel_lineal',Float32,callback_vel)
    r = rospy.Rate(2) #2hz 

    
    while not rospy.is_shutdown():
       
        # Sentido del giro
        if giro > 0:
            dir_giro = "g"         # Giro derecha

        if giro < 0:
            dir_giro = "G"     # Giro Izquierda
        
        # Tiempo y velocidad de giro
        vel_giro = 200
        giro = abs(giro)
        tiempo_giro = (giro*1000) / vel_giro
        tiempo_giro = int(tiempo_giro) #remove decimals
        
        # Mover robot linealmente
        # dir_avance = "A"
        # tiempo_avance = (distancia*1000) / vel_avance # LUIS: POR QUE POR 1000
        tiempo_avance = distancia / vel_avance # LUIS: POR QUE POR 1000
        tiempo_avance = int(tiempo_avance) #remove decimals
        
        # Enviar a Arduino
#       info_tray = "T,g,200,200,200"
        # comando = input('Introduce un comando: ') #Input LUIS: CREO QUE ES DEL DE TELECONTROL
        info_tray = "T,"+str(dir_giro)+","+str(vel_avance)+","+str(tiempo_giro)+","+str(tiempo_avance)
        #info_tray = "w"
        print(info_tray)
        # arduino.write(info_tray.encode())
        
        # Activa movimiento
        mov = 1
        
#          Aqui se tiene que esperar hasta que Arduino envie informacion mov = 0
         # while arduino.inWaiting() < 0:
            # Comprueba si hay obstaculos
              # cambio = guiado.controlreactivo()
            
            # Comprueba si finalizo trayecto
              # mov = arduino.read()
            
              # if mov == 0 or cambio == True:
                  # break
    self.arduino.close()

if __name__ == '__main__':
    try:
        # arduino = serial.Serial('/dev/ttyUSB0', 9600)
        trayectoria()
    except rospy.ROSInterruptException: pass


##PRUEBAS
# arduino = serial.Serial('/dev/ttyUSB0', 9600)
# #trayectoria()
# controlreactivo()
//#include <ISR_Timer.h>

#include <PinChangeInt.h>
#include <PinChangeIntConfig.h>
#define _NAMIKI_MOTOR	 //for Namiki 22CL-103501PG80:1

#include <fuzzy_table.h>
#include <PID_Beta6.h>

#ifndef MICROS_PER_SEC
#define MICROS_PER_SEC 1000000
#endif


/*
            \                    /
   wheel1   \                    /   wheel4
   Left     \                    /   Right
    
    
                              power switch
    
            /                    \
   wheel2   /                    \   wheel3
   Right    /                    \   Left
*/


// DEFINIR RUEDAS
// Rueda 1 (UL)
int R1= 11; 
int D1= 12;
// Rueda 2 (LL)
int R2= 3; 
int D2= 2; 
// Rueda 3 (LR)
int R3= 10; 
int D3= 7;
// Rueda 4 (UR)
int R4= 9; 
int D4= 8;

// Variables
unsigned long t = 0;
int CR = 0;
int mov = 0;
char tipo;

//LEDs control
int led_tray = 4;
int led_cr = 5;



void setup() {
  // Comunicación Serial  
  Serial.begin(115200);
  
  // Definir pines como salida
  pinMode(R1,OUTPUT); 
  pinMode(D1,OUTPUT); 
  pinMode(R2,OUTPUT); 
  pinMode(D2,OUTPUT); 
  pinMode(R3,OUTPUT); 
  pinMode(D3,OUTPUT); 
  pinMode(R4,OUTPUT); 
  pinMode(D3,OUTPUT);   
  pinMode(led_tray, OUTPUT);
  pinMode(led_cr, OUTPUT);

  mov_robot('P',0);

  // Avisar que se inicializa el trayecto
  Serial.println(mov);
}

// Probar a hacer dos vectores para cada movimiento
void loop() {
  
  // Obtiene los valores de la raspberry
  String vector = "";
  
  
  if (Serial.available()>0){
    
    vector = Serial.readString();
    //vector = "T,g,100,900,3000";      // Trayectoria punto a punto
    //vector = "C,g,20";                // Control Reactivo
    
    // Tipo de movimiento
    tipo = vector.charAt(0);

    if (tipo == 'T'){
        CR = 0;    
        trayectoria(vector);   
    }
    
    if (tipo == 'C'){
        CR = 1;
        controlreactivo(vector);
    } 
      
  }  
}


void trayectoria(String vector){
     
    // Comienza el movimiento
    mov = 1;
    digitalWrite(led_tray,HIGH); //LED blanco trayectoria
    
    // Direcciones
    char dir_giro = vector.charAt(2);
    char dir_avance = 'A';

    // Velocidades
    int cont = 4;
    for (int i=4;1<vector.length();i++){
      if (vector.charAt(cont) == ','){
        break;
      }
      cont++;
    }
    cont = cont + 1;
    String vel = vector.substring(4,cont);
    int vel_avance = vel.toInt();
    
    int vel_giro = 100;
     
    // Tiempo
    int inicial = cont;
    for (int i=cont;1<vector.length();i++){
      if (vector.charAt(cont) == ','){
        break;
      }
      cont++;
    }
    cont = cont + 1;
    String tiempoG = vector.substring(inicial,cont);
    int tiempo_giro = tiempoG.toInt();

    inicial = cont;
    cont = vector.length();
    String tiempoA = vector.substring(inicial,cont);
    int tiempo_avance = tiempoA.toInt();

    if (CR == 0){
      
      mov_robot(dir_giro,vel_giro);  
      
      t  = millis();
      while(1){
        if (Serial.available()>0){
          vector = Serial.readString();
          
          // Tipo de movimiento
          tipo = vector.charAt(0);
      
          if (tipo == 'C'){
              CR = 1;
              digitalWrite(led_tray,LOW);
              controlreactivo(vector);
              goto salida_trayectoria;
          } 
        }
         
        if ((millis() - t) > tiempo_giro){
           break;
        }      
      }  
    }
    
    if (CR == 0){
      mov_robot(dir_avance,vel_avance);
    
      t  = millis();
      while(1){
        if (Serial.available()>0){
          vector = Serial.readString();
          
          // Tipo de movimiento
          tipo = vector.charAt(0);
      
          if (tipo == 'C'){
              CR = 1;
              digitalWrite(led_tray,LOW);
              controlreactivo(vector);
              goto salida_trayectoria;
          } 
        }
         
        if ((millis() - t) > tiempo_avance){
          break;
        } 
      } 
    }

    // Avisar que se completo el trayecto
    mov = 0;
    Serial.println(mov);

    salida_trayectoria:;
    digitalWrite(led_tray,LOW);
  
}

      
void controlreactivo(String vector){
  
    // Comienza el movimiento
    //mov = 1;
    digitalWrite(led_cr, HIGH); //LED rojo
    // Tipo de movimiento
    tipo = vector.charAt(0);
    
    if (tipo == 'C'){
      // Direcciones
      char dir_cr = vector.charAt(2);
  
      // Velocidades
      int inicial = 4;
      int cont = vector.length();
      String velA = vector.substring(inicial,cont);
      int vel_cr = velA.toInt();
      
      // Tiempo
      int tiempo_cr = 600; //este valor tiene que estar sincronizado con el threading en la rbpi
  
      mov_robot(dir_cr, vel_cr);
      
      t  = millis();
      while(1){
        if ((millis() - t) > tiempo_cr){
          break;
        }    
      }
      // Avisar que se completo el trayecto
      //mov = 0;
      //Serial.println(mov);
    }   
    digitalWrite(led_cr, LOW);
}


void mov_robot (char dir, int vel) 
{
  switch (dir) {
        // Movimiento de avance
        case 'A':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de retroceso
        case 'R':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de rotación izquierda
        case 'G':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,LOW);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento de rotación derecha
        case 'g':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento diagonal izquierda
        case 'D':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,vel);
            analogWrite(R3,0);
            analogWrite(R4,vel);
            break;
        // Movimiento diagonal derecha
        case 'd':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,LOW);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,0);
            analogWrite(R3,vel);
            analogWrite(R4,0);
            break;
        // Movimiento horizontal izquierda
        case 'H':
            // Dirección de las ruedas
            digitalWrite(D1,LOW);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,LOW);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Movimiento horizontal derecha
        case 'h':
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,LOW);
            digitalWrite(D3,LOW);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,vel);
            analogWrite(R2,vel);
            analogWrite(R3,vel);
            analogWrite(R4,vel);
            break;
        // Parada del robot
        case 'P':            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,0);
            analogWrite(R3,0);
            analogWrite(R4,0);
            break;
        default:
            // Dirección de las ruedas
            digitalWrite(D1,HIGH);
            digitalWrite(D2,HIGH);
            digitalWrite(D3,HIGH);
            digitalWrite(D4,HIGH);
            
            // Velocidad de las ruedas
            analogWrite(R1,0);
            analogWrite(R2,0);
            analogWrite(R3,0);
            analogWrite(R4,0);
            break;
        }
}

#!/usr/bin/python3

### Librerias
import rospy
import math
from std_msgs.msg import UInt8, Float32
from geometry_msgs.msg import Point

# ((Si haceis los subscribers con globales))
busqueda = 0
sentido_lider = -1 # 0 izq <-, 1 dcha ->, -1 No info
sentido_persA = -1 # 0 izq <-, 1 dcha ->, -1 No info
objetivo = 1

def busqueda_lider(num_i,giro):
    if num_i < 405/giro:
        theta = giro/360*2*math.pi
        d_planif = 0
        vel = 0
    else:
        theta = 0
        d_planif = 2
        vel = 0.5
    return theta, d_planif, vel

def giro_robot(giro, sentido):
    if sentido == 0: # Izquierda
        theta = - giro/360*2*math.pi
    else: #Derecha 
        theta = giro/360*2*math.pi
    return theta

def callback_objetivo(msg):
    global objetivo
    rospy.loginfo("Objetivo: {}".format(msg.data))
    objetivo = msg.data

def callback_busqueda(msg):
    global busqueda
    rospy.loginfo("Busqueda: {}".format(msg.data))
    busqueda = msg.data

def callback_sentP(msg):
    global sentido_persA
    rospy.loginfo("Sentido perseguidor: {}".format(msg.data))
    sentido_persA = msg.data

def callback_sentL(msg):
    global sentido_lider
    rospy.loginfo("Sentido lider: {}".format(msg.data))
    sentido_lider = msg.data

def busqueda_nodo(): # Lo que sea esto...
    global busqueda, sentido_lider, sentido_persA, objetivo
    # Subscribers: topic /busqueda, /sentido_lider, /sentido_persA /objetivo
    # Publishers: angulo, distancia, /perseguidor_b/vel_lineal
        
    rospy.init_node('busqueda', anonymous=True)
    sub_sentidoL = rospy.Subscriber('/lider/sentido',UInt8,callback_sentL)
    sub_sentidoP = rospy.Subscriber('/perseguidor_a/sentido',UInt8,callback_sentP)
    subO =  rospy.Subscriber('/sentido_persA',UInt8,callback_objetivo)
    subB = rospy.Subscriber('/busqueda',UInt8,callback_busqueda)

    pubDist = rospy.Publisher('/distancia',Float32,queue_size=10) # YA NO ES TRAYECTORIA ES distancia y angulo
    pubAng = rospy.Publisher('/angulo',Float32,queue_size=10)
    pubVel = rospy.Publisher('/perseguidor_b/vel_lineal',Float32,queue_size=10)
    r = rospy.Rate(2) #2hz    
    lider = 1
    perseguidor = 3
    giro = 7.5 #(Para que sea multiplo de 15º)
    
    num_i = 0
    num_g = 0
    while not rospy.is_shutdown():
        if busqueda:
            if objetivo == lider:
                sentido = sentido_lider
            else:
                sentido = sentido_persA
                
            ### Algoritmo
            if sentido != -1 and num_g < 360/giro: # Hay info
                theta = giro_robot(giro, sentido)
                d_planif = 0
                vel = 0
                num_g+=1
            else:
                sentido_lider, sentido_persA = -1, -1 # No hay info
                theta, d_planif, vel = busqueda_lider(num_i,giro)
                num_i = num_i + 1 if num_i<405/giro else 0
                num_g = 0

            ### Publicar datos en ROS: theta d_planif y velocidad
            dist_plan = Float32()
            dist_plan.data = d_planif
            pubDist.publish(dist_plan)    
            ang_plan = Float32()
            ang_plan.data = theta
            pubAng.publish(ang_plan)   
            velocidad = Float32()
            velocidad.data = vel
            pubVel.publish(velocidad)
            rospy.loginfo('Distancia planificada: {}'.format(dist_plan.data))
            rospy.loginfo('Angulo: {}'.format(ang_plan.data))
            rospy.loginfo('Velocidad: {}'.format(velocidad.data))
                
        r.sleep()

if __name__ == '__main__':
    try:
        busqueda_nodo()
    except rospy.ROSInterruptException: pass
        

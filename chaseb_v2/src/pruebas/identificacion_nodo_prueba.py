#!/usr/bin/env python
import rospy
import time
from chaseb.msg import BoundingBox
from std_msgs.msg import String, UInt8

### Funciones auxiliares formato
def bboxInt2String(bbox):
    # Tranforma [85,60,0,0,1] a '85,60,0,0,1'
    mystr = ''
    for elem in bbox:
        mystr = mystr + str(elem) + ','
    return mystr[0:-1]

def adaptHuskylens(obj):
    # Adapta el formato de Huskylens
    detection = []
    if(type(obj)==list):
        for elem in obj:
            dics = elem.__dict__
            detection.append([dics['x'],dics['y'],dics['width'],dics['height'],dics['ID']])
    else:
        dics = obj.__dict__
        detection.append([dics['x'],dics['y'],dics['width'],dics['height'],dics['ID']])
    return detection

def identificacion():
    rospy.init_node('identificacion', anonymous=True)
    # hl = HuskyLensLibrary("I2C","", address=0x32) 
    
    # add topic 0 o 1 para indicar si detectamos algo o no para iniciar el algoritmo de busqueda del lider
    pubL = rospy.Publisher('/lider/bbox',String,queue_size=10)
    pubP = rospy.Publisher('/perseguidor_a/bbox',String,queue_size=10)
    pubS = rospy.Publisher('/saboteador/bbox',String,queue_size=10)
    pub_sentidoL = rospy.Publisher('/lider/sentido',UInt8,queue_size=10)
    pub_sentidoP = rospy.Publisher('/perseguidor_a/sentido',UInt8,queue_size=10)
    pub_busqueda = rospy.Publisher('busqueda',UInt8,queue_size=10)
    r = rospy.Rate(2) #2hz
    
    #Definir IDs y rangos
    cod_perseguidorA = 2 
    rangox = 80 # 1/4 del ancho de la pantalla
    rangoy = 60 # 1/4 del alto de la pantalla
    
    # Inicializar posiciones previa
    prev_bbox_sabot = [0,0,0,0,0]
    prev_bbox_lider = [0,0,0,0,0]
    prev_bbox_persA = [0,0,0,0,0]
    
    while not rospy.is_shutdown():
        ### Obtener datos desde sensor vision Raspberry
        # try: 
        #     detection = hl.requestAll()
        # except:
        #     detection = []
        #     
        # detection = adaptHuskylens(detection)
        # Ej entrada: 
        detection = [[85,60,20,20,1], [300,222,20,20,1], [160,120,20,20,2]]
        #detection = []
        #detection = [[300,222,20,20,4]]
        
        for bbox in detection:
            if bbox[4] == cod_perseguidorA:
                bbox[4] = 3 # Perseguidor
            else:
                # Distincion entre lider y saboteador
                if prev_bbox_lider[4] !=0:  # Lider ya identificado
                    if (prev_bbox_lider[0] - rangox < bbox[0] < rangox + prev_bbox_lider[0]
                        and (prev_bbox_lider[1] - rangoy < bbox[1] < rangoy + prev_bbox_lider[1])):
                        bbox[4] = 1 # Lider
                        prev_bbox_lider = [0,0,0,0,1]
                    else:
                        bbox[4] = 2 # Saboteador
                        prev_bbox_sabot = [0,0,0,0,2]
                else:
                    if prev_bbox_sabot[4] !=0: # Saboteador ya identificado
                        if (prev_bbox_sabot[0] - rangox < bbox[0] < rangox + prev_bbox_sabot[0]
                            and (prev_bbox_sabot[1] - rangoy < bbox[1] < rangoy + prev_bbox_sabot[1])):
                            bbox[4] = 2 # Saboteador
                            prev_bbox_sabot = [0,0,0,0,2]
                        else:
                            bbox[4] = 1 # Lider
                            prev_bbox_lider = [0,0,0,0,1]
                    else:
                        bbox[4] = 1 # Lider
                        prev_bbox_lider = [0,0,0,0,1]

        lider = False
        persA = False

        for elem in detection:
            bbox_str = bboxInt2String(elem[0:5])
            if bbox_str[-1] == '1':
                msgL = bbox_str
                pubL.publish(msgL) # deteccion lider
                rospy.loginfo('Lider Detectado')
                lider = True
                prev_bbox_lider = elem
                sentido_lider = 0 if elem[0] < prev_bbox_lider[0] else 1
                pub_sentidoL.publish(sentido_lider)
            elif bbox_str[-1] == '2':
                msgS = bbox_str
                pubS.publish(msgS)  # deteccion sabot
                rospy.loginfo('Saboteador Detectado')
                prev_bbox_sabot = elem
            else:
                msgP = bbox_str
                pubP.publish(msgP) # deteccion perseguidor A
                rospy.loginfo('Perseguidor Detectado')
                persA = True
                prev_bbox_persA = elem
                sentido_persA = 0 if elem[0] < prev_bbox_persA[0] else 1
                pub_sentidoP.publish(sentido_persA) 
        
        if not lider and not persA:
            pub_busqueda.publish(1) # Inicializar algoritmo de busqueda
        else:
            pub_busqueda.publish(0)
            
        r.sleep()

if __name__ == '__main__':
    try:
        identificacion()
    except rospy.ROSInterruptException: pass

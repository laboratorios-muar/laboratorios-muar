#!/usr/bin/env python

import rospy
import time
from chaseb.msg import MiPose
from std_msgs.msg import String
from math import pi as PI

def localizacion():
    rospy.init_node('localizacion', anonymous=True)

    pub = rospy.Publisher('/perseguidor_b/pose',MiPose,queue_size=10)
    r = rospy.Rate(2) #2hz
    while not rospy.is_shutdown():
        pose_b = MiPose()
        pose_b.timestamp = rospy.get_rostime()
        pose_b.x = 0.0
        pose_b.y = 0.0
        pose_b.theta = PI/2
        pub.publish(pose_b)
        r.sleep()

if __name__ == '__main__':
    try:
        localizacion()
    except rospy.ROSInterruptException: pass